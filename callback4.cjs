const fs = require("fs"); //importing fs
const problem1 = require("./callback1.cjs"); //importing problem1 function
const problem2 = require("./callback2.cjs"); //importing problem2 function
const problem3 = require("./callback3.cjs"); //importing problem3 function

const boardFilePath = "../boards.json";
function problem4(callback) {
  //creating function
  setTimeout(() => {
    //used setTimeOut to set time for execution

    fs.readFile(boardFilePath, "utf8", (error, data) => {
      //reading file using readFile method
      if (error) {
        callback(error);
      } else {
        const boardData = JSON.parse(data); //converting json to object type
        let thonasId;
        boardData.forEach((element) => {
          //iterating over boardData
          if (element.hasOwnProperty("name") && element.hasOwnProperty("id")) {
            if (element.name === "Thanos") {
              //if find thanos
              thonasId = element.id; //assigning id
              result1 = problem1(thonasId, (error, data) => {
                //calling problem1 function
                if (error) {
                  callback(error);
                } else {
                  callback(null, data); //calling cb function
                }
              });
            }
          }
        });
        problem2(thonasId, (error, data) => {
          //calling problem2 function
          if (error) {
            callback(error);
          } else {
            let mindId;
            data[thonasId].find((value) => {
              //finding name
              if (value.name === "Mind") {
                // if name matches
                mindId = value.id; //assigning id
              }
            });
            problem3(mindId, (error2, data2) => {
              //calling problem3 function
              if (error2) {
                callback(error);
              } else {
                callback(null, data); //firstly calling cb with data
                callback(null, data2); //after that calling cb with data2
              }
            });
          }
        });
      }
    });
  }, 3000);
}
module.exports = problem4; //exporting function
