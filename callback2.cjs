const fs = require("fs"); //importing fs
const filePath = "../lists.json"; //assigning path

function boardIdList(boardId, callback) {
  //creating function
  let result = {};
  setTimeout(() => {
    //used setTimeOut to set time for execution
    fs.readFile(filePath, "utf8", (error, data) => {
      //reading file using readFile method
      if (error) {
        callback(error);
      } else {
        const list = JSON.parse(data); //converting json to object type
        if (list.hasOwnProperty(boardId)) {
          result[boardId] = list[boardId]; //assigning boardId data
        }
        callback(null, result); //calling callback function
      }
    });
  }, 3000);
}
module.exports = boardIdList; //exporting function
