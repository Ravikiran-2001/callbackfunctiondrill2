const fs = require("fs"); //importing fs
const filePath = "../boards.json"; //assigning path
function boardInformation(boardId, callback) {
  //creating function
  setTimeout(() => {
    //used setTimeOut to set time for execution
    fs.readFile(filePath, "utf8", (error, data) => {
      //reading file using readFile method
      if (error) {
        callback(error);
        return;
      }
      const board = JSON.parse(data); //converting json to object type
      const result = board.find((value) => value.id === boardId); //if id is same it will return object
      callback(null, result); //calling callback function
    });
  }, 3000);
}
module.exports = boardInformation; //exporting function
