const fs = require("fs"); //importing fs
const problem1 = require("./callback1.cjs"); //importing problem1 function
const problem2 = require("./callback2.cjs"); //importing problem2 function
const problem3 = require("./callback3.cjs"); //importing problem3 function

const boardFilePath = "../boards.json";
function problem6(callback) {
  //creating function
  setTimeout(() => {
    //used setTimeOut to set time for execution
    fs.readFile(boardFilePath, "utf8", (error, data) => {
      //reading file using readFile method
      if (error) {
        callback(error);
      } else {
        const boardData = JSON.parse(data); //converting json to object type
        let thonasId;
        boardData.forEach((element) => {
          //iterating over boardData
          if (element.hasOwnProperty("name") && element.hasOwnProperty("id")) {
            if (element.name === "Thanos") {
              //if find thanos
              thonasId = element.id; //assigning id
              result1 = problem1(thonasId, (error, data) => {
                //calling problem1 function
                if (error) {
                  callback(error);
                } else {
                  callback(null, data); //calling cb function
                }
              });
            }
          }
        });
        problem2(thonasId, (error1, data1) => {
          //calling problem2 function
          if (error1) {
            callback(error1);
          } else {
            callback(data1); //calling cb with data1
            data1[thonasId].forEach((value) => {
              //iterating inside data1
              problem3(value.id, (error, data) => {
                //calling problem3 function
                if (error) {
                  callback(error);
                } else {
                  callback(null, data); //calling cb function with data
                }
              });
            });
          }
        });
      }
    });
  }, 3000);
}
module.exports = problem6; //exporting function
