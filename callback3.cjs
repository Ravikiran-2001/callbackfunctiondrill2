const fs = require("fs"); //importing fs
const filePath = "../cards.json"; //assigning path
function allCards(listId, callback) {
  //creating function
  let result = {};
  setTimeout(() => {
    //used setTimeOut to set time for execution
    fs.readFile(filePath, "utf8", (error, data) => {
      //reading file using readFile method
      if (error) {
        callback(error);
      } else {
        const list = JSON.parse(data); //converting json to object type
        if (list.hasOwnProperty(listId)) {
          result[listId] = list[listId]; //assigning list id data
        }
        callback(null, result); //calling callback function
      }
    });
  }, 3000);
}
module.exports = allCards; //exporting function
