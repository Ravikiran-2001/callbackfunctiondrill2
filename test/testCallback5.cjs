const problem5 = require("../callback5.cjs"); //importing function

problem5((error, data) => {
  //calling function and implementing callBack function
  if (error) {
    console.log(error);
  } else {
    console.log(data);
    return;
  }
});
