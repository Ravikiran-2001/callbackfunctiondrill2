const boardInformation = require("../callback1.cjs"); //importing function

boardInformation("abc122dc", (error, data) => {
  //calling function and implementing callBack function
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});
