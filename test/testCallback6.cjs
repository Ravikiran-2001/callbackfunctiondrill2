const problem6 = require("../callback6.cjs"); //importing function

problem6((error, data) => {
  //calling function and implementing callBack function
  if (error) {
    console.log(error);
  } else {
    console.log(data);
    return;
  }
});
