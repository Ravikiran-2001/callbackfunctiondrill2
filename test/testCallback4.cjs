const problem4 = require("../callback4.cjs"); //importing function

problem4((error, data) => {
  //calling function and implementing callBack function
  if (error) {
    console.log(error);
  } else {
    console.log(data);
    return;
  }
});
